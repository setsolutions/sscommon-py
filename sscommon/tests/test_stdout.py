#==============================================================================#
# Set Solutions Common Libraries                                               #
# Python Functions                                 Logging and Standard Output #
#==============================================================================#
# Python Functions for Logging and Standard Output                             #
# : - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - #
# : Logging and Standard Output                                       messages #
#==============================================================================#


#------------------------------------------------------------------------------#
# Python Functions for Logging and Standard Output                             #
#------------------------------------------------------------------------------#
#
#~~ Logging and Standard Output ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Handle the standard messages generated by various procedures in the software
#-----------------------------------------------------------------------------
def test_messages(capsys):
    #
    # Import the module and functions relevant to this particular set of tests
    from sscommon.stdout import messages
    #
    # Assert the relevant conditions indicating either test success or failure
    assert messages({"msg": "Testing 123"}, "err")
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#------------------------------------------------------------------------------#
